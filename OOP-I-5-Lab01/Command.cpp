#include "Command.h"
#include <iostream>
//�imdilik boyutu 18 olarak verildi
string Command::command_type[18] = { "start_engine;","add_fuel_tank","fill_tank","connect_fuel_tank_to_engine",
"remove_fuel_tank","disconnect_fuel_tank_from_engine","give_back_fuel","open_valve","wait","list_fuel_tanks;",
"print_fuel_tank_count;","list_connected_tanks;","print_total_fuel_quantity;","print_total_consumed_fuel_quantity;",
"print_tank_info","close_valve","stop_simulation;","stop_engine;" };

Command::Command() {
	command = "";
	parametre_1 = "";
	parametre_2 = "";
}
void Command::separate(const string& line) {
	int space_count = 0;

	for (int i = 0; i < line.size(); i++)
	{
		if (line[i] == ' ') { space_count++; i++; }

		if (space_count == 0) { this->command += line[i]; }
		else if (space_count == 1) { this->parametre_1 += line[i]; }
		else if (space_count == 2) { this->parametre_2 += line[i]; }
	}
}
bool Command::is_Command()const {
	bool control = false;

	int length_Of_Command = command_type->size();
	int length_Of_Parametre1 = parametre_1.size();
	int length_Of_Parametre2 = parametre_2.size();

	if (parametre_1 != "" && parametre_2 == "") {
		if (parametre_1[length_Of_Parametre1 - 1] != ';') {
			return false;
		}
		else {
			length_Of_Parametre1--;
		}
	}
	else if (parametre_1 != "" && parametre_2 != "") {
		if (parametre_2[length_Of_Parametre2 - 1] != ';') {
			return false;
		}
		else {
			length_Of_Parametre2--;
		}
	}

	for (int i = 0; i < length_Of_Command; i++)
	{
		if (this->command == command_type[i]) {  // Ge�erli komut girildi mi kontrol�
			control = true;
			break;
		}
	}

	for (int i = 0; i < length_Of_Parametre1; i++) {   //Rakamlardan olu�tu�unu kontrol etme
		if (int(parametre_1[i]) < 48 || int(parametre_1[i]) > 57) {
			return false;
		}
	}

	for (int i = 0; i < length_Of_Parametre1; i++) {   //Rakamlardan olu�tu�unu kontrol etme
		if (int(parametre_1[i]) < 48 || int(parametre_1[i]) > 57) {
			return false;
		}
	}
	return control;
}
string Command::get_Command()const {
	return this->command;
}
string Command::get_parametre1()const {
	return this->parametre_1;
}
string Command::get_parametre2()const {
	return this->parametre_2;
}