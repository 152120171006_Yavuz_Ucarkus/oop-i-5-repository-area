#pragma once
#pragma once
#include <string>
#include <vector>
using namespace std;

class Command {

private:
	string  command;
	string parametre_1;
	string parametre_2;
public:
	static string command_type[18]; //input.txt format�na bakarak �imdilik 18 farkl� komut olarak belirlendi
	Command();
	void separate(const string&);
	bool is_Command()const;
	string get_Command()const;
	string get_parametre1()const;
	string get_parametre2()const;

};