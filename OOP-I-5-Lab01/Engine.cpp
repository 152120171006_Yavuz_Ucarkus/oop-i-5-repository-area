#include "Engine.h"
#include <ctime>
#include <iostream>
#include "main.h"

 double Engine::total_consume_fuel;
 Engine* Engine::instance = NULL;
 Engine Engine::getInstance() {
	 if (Engine::instance == NULL) {
		 instance = new Engine();
	 }
	 return (*instance);
 }

Engine::Engine() {
	this->status = false;
	this->connected_tanks = new vector<Tank>;
	this->internal_tank = new Tank(55.0);   // Capacity 55   
	total_consume_fuel = 0;  // Ba�lang�� de�eri 0 
}

void Engine::start_engine()
{
	this->status = true;
	Output_File::print("output.txt", "Engine started");
}

void Engine::stop_engine()
{   
	this->status = false;
	Output_File::print("output.txt", "Engine stopped");
}

void Engine::give_back_fuel(double quantity)
{
	int length = this->connected_tanks->size();
	if (length < 1) { cout << "ERROR !" << endl; }

	Tank* min = &Engine::getInstance().getTanks().at(0);
	
	while (Engine::getInstance().get_internal().getFuelQuantity() != 0) {
		for (int i = 1; i < length; i++)
		{
			if (min->getFuelQuantity() > Engine::getInstance().getTanks().at(i).getFuelQuantity()) {
				min = &Engine::getInstance().getTanks().at(i);
			}
		}
		if ((min->getCapacity() - min->getFuelQuantity() >= quantity)) {
			min->setFuelQuantity(quantity + min->getFuelQuantity());
			quantity = 0;
		}
		else {
			quantity = quantity - ((min->getCapacity() - min->getFuelQuantity()));
			min->setFuelQuantity(min->getCapacity());
		}
	}
}

double Engine::getFuelPersecond()const
{
	return fuel_per_second;
}
Tank Engine::get_internal()const {
	return (*internal_tank);
}
double Engine::get_total_consume_fuel()const {
	return this->total_consume_fuel;
}

void Engine::Update()
{
	cout << "Simulation stopped" << endl;
	Output_File::print("output.txt", "Simulation stopped");
}

bool Engine::getStatus()
{
	return this->status;
}

void Engine::setStatus(bool status)
{
	this->status = status;
}

vector<Tank> Engine::getTanks()const
{
	return vector<Tank>();
}
void  Engine::absorb_fuel(double quantity)
{   
	if ((main::tank_op.print_total_fuel_quantity() - this->internal_tank->getFuelQuantity()) < quantity && this->internal_tank->getFuelQuantity() < 20.0) {
		this->stop_engine();  // Kullanacak benzin kalmad�ysa
		return;
	}
	if (this->status == false) {
		cout << "Motor kapal� oldu�undan yak�t t�ketilemedi";
		return;
	}
	srand(time(0));
	if (this->internal_tank->getFuelQuantity() < 20.0 && this->connected_tanks->size() > 0) {
		
		int rand_tank = rand() % (this->connected_tanks->size() - 0 + 1) + 0;
		
		while (quantity != 0.0) {
			
			if (this->connected_tanks->at(rand_tank).getFuelQuantity() != 0) { //Random tank t�m quantityi kar��l�yor
				if (this->connected_tanks->at(rand_tank).getFuelQuantity() >= quantity) {
					this->connected_tanks->at(rand_tank).setFuelQuantity(this->connected_tanks->at(rand_tank).getFuelQuantity() - quantity);
					this->total_consume_fuel += quantity;
					quantity = 0.0;
					return;
				}
				else { //Random tank t�m quantityi kar��layamay�p b�t�n benzinini t�ketiyor sonra d�ng�ye tekrar giriyor quantityi 0 lamak i�in
					quantity = quantity - this->connected_tanks->at(rand_tank).getFuelQuantity();
					this->total_consume_fuel += this->connected_tanks->at(rand_tank).getFuelQuantity();
					this->connected_tanks->at(rand_tank).setFuelQuantity(0.0);
				}
			}
			rand_tank = rand() % (this->connected_tanks->size() - 0 + 1) + 0;
		}
	}
	else if (this->internal_tank->getFuelQuantity() >= 20.0) {
		this->internal_tank->setFuelQuantity(this->internal_tank->getFuelQuantity() - quantity);
		this->total_consume_fuel += quantity;
		return;
	}
	else { 
		// internal tank 20 den k���k ve motora ba�ka ba�l� tank bulunmama durumu
		this->stop_engine();  // Kullanacak benzin kalmad�ysa
		return;
	}
}

