#pragma once
#include <vector>
#include "Tank.h"
#include "Observer.h"
class Engine:public Observer
{
private:
	const double fuel_per_second = (double)5.5;
	bool status;  
	vector<Tank>* connected_tanks;   
	Tank* internal_tank;      
	static double total_consume_fuel;
	static Engine* instance;
	Engine();
public:
	static Engine getInstance();
	void start_engine();
	void stop_engine();
	void absorb_fuel(double quantity);
	void give_back_fuel(double quantity);
	double getFuelPersecond()const;
	bool getStatus();
	void setStatus(bool status);
	vector<Tank> getTanks()const;
	Tank get_internal()const;
	double get_total_consume_fuel()const;
	void Update();
};

