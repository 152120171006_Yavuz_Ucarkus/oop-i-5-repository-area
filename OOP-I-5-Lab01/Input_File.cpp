#include "Input_File.h"
#include <fstream>
#include <iostream>
void Input_File::read_file(string filename, vector<Command> &commands)
{
	ifstream read(filename);
	string line;

	if (read.is_open()) {
		while (getline(read,line))
		{   
			Command c;
			c.separate(line);
			if (c.is_Command() == true) { commands.push_back(c); }
			else { cout << "Gecersiz komut : " << line << endl; }
			if (line == "stop_simulation;") { break; } 
		}
	}
	else {
		cout << "Bu isimde Dosya bulunamadi !";
	}
}
