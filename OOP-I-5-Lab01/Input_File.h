#pragma once
#include <string>
#include <vector>
#include "Command.h"

using namespace std;
class Input_File
{
public:
	static void read_file(string filename, vector<Command> &commands) ;
};

