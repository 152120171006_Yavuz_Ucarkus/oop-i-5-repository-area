#include "Observable.h"

Observable::Observable()
{
	this->Observers = new vector<Observer*>();
}

void Observable::addObserver(Observer* obs)
{
	Observers->push_back(obs);
}

void Observable::notify()
{
	int length = Observers->size();
	for (int i = 0; i < length; i++)
	{
		Observers->at(i)->Update();
	}
}
