#pragma once
#include "Observer.h"
#include <vector>

using namespace std;

class Observable {
private:
	vector<Observer*>* Observers;
public:
	Observable();
	void addObserver(Observer*);
    void notify();
};