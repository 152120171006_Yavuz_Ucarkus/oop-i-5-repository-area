#include "Output_File.h"
#include <fstream>


void Output_File::print(string filename,string text)
{
	ofstream dosyaYaz(filename);

	if (dosyaYaz.is_open()) {
		dosyaYaz << text;
		dosyaYaz.close();
	}
}

void Output_File::createFile(string filename)
{
	ofstream create(filename);
	create.close();
}
