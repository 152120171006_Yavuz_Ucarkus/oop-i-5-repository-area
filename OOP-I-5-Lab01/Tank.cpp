#include "Tank.h"
#include <cstddef>
#include <iostream>
#include "main.h"
using namespace std;
int Tank::id = -1;  // Initialize id

Tank::Tank(double capacity, double fuel_quantity) {
	id++;
	this->capacity = capacity;
	this->fuel_quantity = fuel_quantity;
	this->valve = new Valve();
	this->valve->setStatus(false); // Kapak kapali halde oluşturuldu
	this->tank_id = id;
	this->is_connected = false;
	main::observable.addObserver(this); //Tank oluştuğunda observable vectorune ekleme yapıldı.
	main::observable.addObserver(valve); //Valve oluştuğunda observable vectorune ekleme yapıldı.
}

int Tank::getTankid() const
{
	return this->tank_id;
}

bool Tank::get_connected()const {
	return this->is_connected;
}
void Tank::set_connected(bool connected) {
	this->is_connected = connected;
}

void Tank::Update()
{
	cout << "Simulation stopped" << endl;
	string txt = "Tank  " + to_string(tank_id);
	txt += " Simulation stopped ";
	Output_File::print("output.txt", txt);

}

double Tank::getCapacity()
{
	return this->capacity;
}

void Tank::setCapacity(double capacity)
{
	this->capacity = capacity;
}

double Tank::getFuelQuantity()
{
	return this->fuel_quantity;
}

void Tank::setFuelQuantity(double fuel_quantity)
{
	this->fuel_quantity = fuel_quantity;
}

Valve Tank::getValve()
{
	return *valve;
}

void Tank::setValve(Valve& valve)
{
	this->valve = &valve;
}

