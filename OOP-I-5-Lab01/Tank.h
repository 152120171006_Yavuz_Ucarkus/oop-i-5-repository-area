#pragma once
#include "Valve.h"
#include <cstddef>
#include"Observer.h"


using namespace std;
class Tank:public Observer
{
private:
	int tank_id;
	double capacity;
	double fuel_quantity;
	Valve* valve;
	static int id;
	bool is_connected;
public:
	Tank(double capacity, double fuel_quantity = 0);
	int getTankid()const;
	double getCapacity();
	void setCapacity(double capacity);
	double getFuelQuantity();
	void setFuelQuantity(double fuel_quantity);
	Valve getValve();
	void setValve(Valve& valve);
	bool get_connected()const;
	void set_connected(bool connected);
	void Update();
};

