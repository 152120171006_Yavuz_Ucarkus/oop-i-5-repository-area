#include "TankOperation.h"
#include "main.h"

void TankOperation::list_fuel_tanks()const
{
	for (int i = 0; i < main::tanks.size(); i++)
	{
		cout << "Listelenen tank id : " << main::tanks.at(i).getTankid() << endl;
		Output_File::print("output.txt", "Tank id : "+ to_string(main::tanks.at(i).getTankid()));
		
	}
}

void TankOperation::remove_fuel_tank(int tank_id)
{   
	Tank* delete_tank;
	for (int i = 0; i < main::tanks.size(); i++)
	{
		if (main::tanks.at(i).getTankid() == tank_id) {
			delete_tank = &main::tanks.at(i);
			main::tanks.erase(main::tanks.begin() + i);
			if (main::tanks.at(i).get_connected()) {
				for (int j = 0; j < Engine::getInstance().getTanks().size(); j++)
				{
					if (Engine::getInstance().getTanks().at(j).getTankid() == tank_id) {
						Engine::getInstance().getTanks().erase(main::tanks.begin() + j); // Ba�l� tank vekt�r�nden silme
					}
				}
			}
			main::tanks.erase(main::tanks.begin() + i); // T�m tank vekt�r�nden silme
			delete(delete_tank);
			Output_File::print("output.txt", "Tank deleted");
			return;
		}
	}
	cout << "Bu id ye sahip bir tank bulunamadi " << endl;
}

void TankOperation::connect_fuel_tank_to_engine(int tank_id)
{
	for (int i = 0; i < main::tanks.size(); i++)
	{
		if (tank_id == main::tanks.at(i).getTankid()) {
			if (main::tanks.at(i).get_connected()) { cout << "This tank already connected to the engine" << endl; return; }
			Engine::getInstance().getTanks().push_back(main::tanks.at(i));
			main::tanks.at(i).set_connected(true);
			Output_File::print("output.txt", "Tank connected engine");
			return;
		}
	}
	Output_File::print("output.txt", "There is no tank with this id");
	cout << "Bu id ye sahip bir tank bulunmamaktadir ! " << endl;
}
void TankOperation::add_fuel_tank(int capacity) {
	Tank* new_tank = new Tank(capacity);
	main::tanks.push_back(*new_tank);
	Output_File::print("output.txt", "Fuel added to tank");
}

void TankOperation::disconnect_fuel_tank_to_engine(int tank_id)
{
	for (int i = 0; i < Engine::getInstance().getTanks().size(); i++)
	{
		if (tank_id == Engine::getInstance().getTanks().at(i).getTankid()) {
			Engine::getInstance().getTanks().at(i).set_connected(false);
			Engine::getInstance().getTanks().erase(Engine::getInstance().getTanks().begin() + i);
			cout << "Baglanti koparildi" << endl;
			Output_File::print("output.txt", "Disconnected tank from engine");
			return;
		}
	}
	Output_File::print("output.txt", "This tank already disconnect");
	cout << "Bu id ye sahip bir tank zaten bagli degil" << endl;
}
void TankOperation::fill_tank(int tank_id, int capacity) {
	for (int i = 0; i < main::tanks.size(); i++)
	{
		if (main::tanks.at(i).getTankid() == tank_id) {
			if (capacity > main::tanks.at(i).getCapacity() - main::tanks.at(i).getFuelQuantity()) {
				main::tanks.at(i).setFuelQuantity(main::tanks.at(i).getCapacity());
				cout << "Tank fullendi.Bir miktar benzin artt� : " << endl;
			}
			else {
				main::tanks.at(i).setFuelQuantity(capacity + main::tanks.at(i).getFuelQuantity());
				cout << "Benzin yuklemesi yapildi " << endl;
			}
			Output_File::print("output.txt", "Tank Filled");
			return;
		}
	}
	cout << "Bu id ye sahip bir tank bulunamad� !" << endl;
	Output_File::print("output.txt", "There is no tank with this id");
}

void TankOperation::break_fuel_tank(int tank_id)
{
}
void TankOperation::print_total_consumed_fuel_quantity()const {
	cout << "Total consume fuel : " << Engine::getInstance().get_total_consume_fuel() << endl;
	Output_File::print("output.txt", "Total consumed quantity : " + to_string(Engine::getInstance().get_total_consume_fuel()));
}

void TankOperation::repair_fuel_tank(int tank_id)
{
}
void TankOperation::print_tank_info(int id)const
{
	for (int i = 0; i < main::tanks.size(); i++)
	{
		if (main::tanks.at(i).getTankid() == id) {
			cout << "Tank Capacity : "; main::tanks.at(i).getCapacity(); cout << endl;
			cout << "Tank Fuel Quantity : "; main::tanks.at(i).getFuelQuantity(); cout << endl;
			cout << "Tank id : "; main::tanks.at(i).getTankid(); cout << endl;
			Output_File::print("output.txt", "Capacity : "+ to_string(main::tanks.at(i).getCapacity()));
			Output_File::print("output.txt", "Tank fuel quantity : " + to_string(main::tanks.at(i).getFuelQuantity()));
			return;
		}
	}
	Output_File::print("output.txt", "There is no tank with this id");
	cout << "B�yle bir tank bulunamad� ";
}
void TankOperation::print_fuel_tank_count()const 
{
	cout << "Tank Count : "; main::tanks.size();  cout << endl;
	Output_File::print("output.txt", "Tank count : "+ to_string(main::tanks.size()));
	
}
void TankOperation::list_connected_tanks()const {
	
	for (int i = 0; i < Engine::getInstance().getTanks().size(); i++)
	{
		cout << "Bagli tank id : " << Engine::getInstance().getTanks().at(i).getTankid() << endl;
		Output_File::print("output.txt", "Connected tank id : " + to_string(Engine::getInstance().getTanks().at(i).getTankid()));
	}	
}
double TankOperation::print_total_fuel_quantity()const {
	double total_fuel = Engine::getInstance().get_internal().getFuelQuantity();

	// Ba�l� tanklar�n benzin miktar� y�kleniyor
	for (int i = 0; i < Engine::getInstance().getTanks().size(); i++)
	{
		total_fuel += Engine::getInstance().getTanks().at(i).getFuelQuantity();
	}
	Output_File::print("output.txt", "Fuel total quantity : " + to_string(total_fuel));
	return total_fuel;
}
