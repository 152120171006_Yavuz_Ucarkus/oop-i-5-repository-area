#pragma once
#include "tank.h"

using namespace std;
class TankOperation
{
public:
	void list_fuel_tanks()const;
	void remove_fuel_tank(int tank_id);
	void connect_fuel_tank_to_engine(int tank_id);
	void disconnect_fuel_tank_to_engine(int tank_id);
	void break_fuel_tank(int tank_id);
	void repair_fuel_tank(int tank_id);
	void print_tank_info(int id)const;
	void print_fuel_tank_count()const;
	void list_connected_tanks()const;
	double print_total_fuel_quantity()const; // Kullan�m avantaj�n� art�rmak i�in d�n�� de�eri double yap�ld�
	void print_total_consumed_fuel_quantity()const;
	void add_fuel_tank(int capacity);
	void fill_tank(int tank_id,int capacity);
};

