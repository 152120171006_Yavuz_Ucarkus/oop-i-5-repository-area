#include "Valve.h"
#include "main.h"


void Valve::open_valve(int tank_id)
{
	for (int i = 0; i < Engine::getInstance().getTanks().size(); i++)
	{
		if (tank_id == Engine::getInstance().getTanks().at(i).getTankid()) {
			if (Engine::getInstance().getTanks().at(i).getValve().getStatus() == false) {
				Engine::getInstance().getTanks().at(i).getValve().setStatus(true);
				Output_File::print("output.txt", "Valve opening");
			}
			else {
				cout << "Bu kapak zaten acik " << endl;
				Output_File::print("output.txt", "This valve already opened");
			}
		}
	}
}

void Valve::close_valve(int tank_id)
{
	for (int i = 0; i < Engine::getInstance().getTanks().size(); i++)
	{
		if (tank_id == Engine::getInstance().getTanks().at(i).getTankid()) {
			if (Engine::getInstance().getTanks().at(i).getValve().getStatus() == true) {
				Engine::getInstance().getTanks().at(i).getValve().setStatus(false);
				Output_File::print("output.txt", "Valve closing");
			}
			else {
				Output_File::print("output.txt", "This valve already closed");
				cout << "Bu kapak zaten kapali " << endl;
			}
		}
	}
}

bool Valve::getStatus() const
{
	return status; // if status == true kapak a��k durumdad�r
}

void Valve::setStatus(bool status)
{
	this->status = status;
}

void Valve::Update()
{
	cout << "Simulation Stopped";
	string txt = "Valve ";
	txt += "Stop Simulation";
	Output_File::print("output.txt", txt);
}
