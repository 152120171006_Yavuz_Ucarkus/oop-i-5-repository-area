#pragma once
#include "Observer.h"
class Valve:public Observer
{
private:
	bool status;

public:
	static void open_valve(int tank_id);
	static void close_valve(int tank_id);
	bool getStatus() const;
	void setStatus(bool status);
	void Update();
};

