#pragma once
#include <iostream>
#include <vector>
#include <stdio.h>
#include "Engine.h"
#include "Input_File.h"
#include "Output_File.h"
#include "TankOperation.h"
#include "Valve.h"
#include <cstdlib>
#include "Command.h"
#include <Windows.h>
#include "Observable.h"
class main {
private:
	void stopSimilation() {

	}
	void wait(int delay) {
		Sleep(delay);
		if (Engine::getInstance().getStatus()) { Engine::getInstance().absorb_fuel(Engine::getInstance().getFuelPersecond() * double(delay / 1000)); } // Motor çalışıyorsa benzin harca
	}
	void running_commands() {
		for (int i = 0; i < commands.size(); i++)
		{
			if (commands.at(i).get_Command() == "start_engine;")
			{
				Engine::getInstance().start_engine();
				
			}
			else if (commands.at(i).get_Command() == "add_fuel_tank") {
				this->tank_op.add_fuel_tank(stoi(commands.at(i).get_parametre1()));
				
			}
			else if (commands.at(i).get_Command() == "fill_tank") {
				this->tank_op.fill_tank(stoi(commands.at(i).get_parametre1()), stoi(commands.at(i).get_parametre2()));
			}
			else if (commands.at(i).get_Command() == "connect_fuel_tank_to_engine") {
				this->tank_op.connect_fuel_tank_to_engine(stoi(commands.at(i).get_parametre1()));
			}
			else if (commands.at(i).get_Command() == "remove_fuel_tank") {
				this->tank_op.remove_fuel_tank(stoi(commands.at(i).get_parametre1()));
			}
			else if (commands.at(i).get_Command() == "disconnect_fuel_tank_from_engine") {
				this->tank_op.disconnect_fuel_tank_to_engine(stoi(commands.at(i).get_parametre1()));
			}
			else if (commands.at(i).get_Command() == "give_back_fuel") {
				//BAKILACAK
			}
			else if (commands.at(i).get_Command() == "open_valve") {
				Valve::open_valve(stoi(commands.at(i).get_parametre1()));
			}
			else if (commands.at(i).get_Command() == "wait") {
				wait(stoi(commands.at(i).get_parametre1()));
				Output_File::print("output.txt", "Waiting");
			}
			else if (commands.at(i).get_Command() == "list_fuel_tanks;") {
				main::tank_op.list_fuel_tanks();
			}
			else if (commands.at(i).get_Command() == "print_fuel_tank_count;") {
				main::tank_op.print_fuel_tank_count();

			}
			else if (commands.at(i).get_Command() == "list_connected_tanks;") {
				main::tank_op.list_connected_tanks();
			}
			else if (commands.at(i).get_Command() == "print_total_fuel_quantity;") {
				main::tank_op.print_total_fuel_quantity();
			}
			else if (commands.at(i).get_Command() == "print_total_consumed_fuel_quantity;") {
				cout << "Total consume fuel : " << Engine::getInstance().get_total_consume_fuel() << endl;
			}
			else if (commands.at(i).get_Command() == "print_tank_info") {
				main::tank_op.print_tank_info(stoi(commands.at(i).get_parametre1()));
			}
			else if (commands.at(i).get_Command() == "close_valve") {
				Valve::close_valve(stoi(commands.at(i).get_parametre1()));
			}
			else if (commands.at(i).get_Command() == "stop_simulation;") {
				main::observable.notify();  
			}
			else if (commands.at(i).get_Command() == "stop_engine;") {
				Engine::getInstance().stop_engine();
			}
			Sleep(1000);  // 1 saniye gecikme
			if (Engine::getInstance().getStatus()) { Engine::getInstance().absorb_fuel(1.0 * Engine::getInstance().getFuelPersecond()); } // Motor çalışıyorsa benzin harca
		}
	}

public:
	static vector<Tank> tanks;
	static TankOperation tank_op;
	static vector<Command> commands;
	static Observable observable;

	main(int argc, char* argv[]) {

		if (argc < 2)
		{
			cout << argv[0] << endl;
			cout << "Input_File bulunamadi" << endl;
			exit(EXIT_FAILURE);
		}
		else if (argc == 2)
		{
			cout << "Argv Output Dosyasi oluşturuldu" << endl;
			Output_File::createFile("output.txt");
		}
		else
		{
			Output_File::createFile(argv[2]);
		}
		Input_File::read_file(argv[1], commands);

		Engine::getInstance();
		running_commands();
	}
};